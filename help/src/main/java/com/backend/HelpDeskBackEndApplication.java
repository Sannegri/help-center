package com.backend;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpDeskBackEndApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(HelpDeskBackEndApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println();
		System.out.println("✅ ✅");
		System.out.println("Help Desk");
		System.out.println("Desenvolvido por: Hiank Wesley");
		System.out.println("LinkedIn: https://www.linkedin.com/in/hiank-wesley/");
		System.out.println("GitHub: https://github.com/Sannegr1");
		System.out.println("Swagger: http://localhost:8080/swagger-ui.html");
		System.out.println("✅ ✅");
	}
}
